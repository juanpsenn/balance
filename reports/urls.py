from django.urls import include
from django.urls import path

urlpatterns = [
    path("movements/", include("reports.movements.urls")),
    path("categories/", include("reports.categories.urls")),
]
