import io
from typing import Iterable

import pandas as pd


def build_xlsx_report(reports: Iterable[dict]):
    """
    Builds a xlsx file from an array of reports in dict format.
    Each report must follows this format:
    {"df": dataframe,
    "sheet": "nombre de hoja en excel"}
    """
    empty = True
    with io.BytesIO() as b:
        # Use the StringIO object as the filehandle.
        writer = pd.ExcelWriter(b, engine="xlsxwriter")
        for report in reports:
            if report["df"] is not None:
                report["df"].to_excel(writer, sheet_name=report["sheet"], index=False)
                empty = False
        writer.save()
        return b.getvalue(), empty
