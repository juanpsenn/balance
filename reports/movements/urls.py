from django.urls import path

from reports.movements import views

urlpatterns = [
    path("", views.MovementListReport.as_view()),
]
