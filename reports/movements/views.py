from django.http import HttpResponse
from rest_framework import serializers
from rest_framework.views import APIView

from reports.movements.selectors import build_movement_list_xlsx_report


class MovementListReport(APIView):
    class FilterSerializer(serializers.Serializer):
        date_time__gte = serializers.DateField(required=False)
        date_time__exact = serializers.DateField(required=False)
        date_time__lte = serializers.DateField(required=False)

    def get(self, request):
        filter_serializer = self.FilterSerializer(data=request.query_params)
        filter_serializer.is_valid(raise_exception=True)

        xlsx_report, empty = build_movement_list_xlsx_report(
            filters=filter_serializer.validated_data
        )
        response = HttpResponse(xlsx_report, content_type="application/ms-excel")
        response["Content-Disposition"] = "attachment; filename=%s" % "movimientos.xlsx"
        return response
