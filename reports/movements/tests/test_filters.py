import pytest

from app.models import Movement
from app.movements.tests.factories.movement import MovementFactory
from app.utils.dates import get_timezone
from reports.movements.filters import MovementListFilter


@pytest.mark.django_db
def test_movement_list_filter():
    _ = MovementFactory(date_time=get_timezone(date_time="2021-01-01"))
    _ = MovementFactory(date_time=get_timezone(date_time="2021-01-06"))
    _ = MovementFactory(date_time=get_timezone(date_time="2021-01-08"))
    filters_lte = {"date_time__lte": "2021-01-05"}
    filters_gte = {"date_time__gte": "2021-01-06"}
    filters_between = {"date_time__gte": "2021-01-01", "date_time__lte": "2021-01-08"}
    qs = Movement.objects.all()

    results_lte = MovementListFilter(filters_lte, qs).qs
    results_gte = MovementListFilter(filters_gte, qs).qs
    results_between = MovementListFilter(filters_between, qs).qs

    assert results_lte.count() == 1
    assert results_gte.count() == 2
    assert results_between.count() == 3
