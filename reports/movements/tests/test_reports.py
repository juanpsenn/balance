import pandas as pd
import pytest

from app.models import Movement
from app.movements.tests.factories.movement import MovementFactory
from reports.movements.selectors import build_category_summary_frame
from reports.movements.selectors import build_movement_list_base_frame
from reports.movements.selectors import build_movement_list_xlsx_report


@pytest.mark.django_db
def test_build_movement_list_frames():
    _ = MovementFactory.create_batch(10, amount=10)
    movements = Movement.objects.all()
    report = build_movement_list_base_frame(movements)
    income_frame, outcome_frame = build_category_summary_frame(report)

    assert isinstance(report, pd.DataFrame)
    assert report.shape[0] == 10

    assert isinstance(income_frame, pd.DataFrame)
    assert isinstance(outcome_frame, pd.DataFrame)

    assert income_frame["Monto"].sum() == 100
    assert outcome_frame["Monto"].sum() == 0


@pytest.mark.django_db
def test_build_movement_list_frames_empty():
    movements = Movement.objects.all()
    report = build_movement_list_base_frame(movements)
    income_frame, outcome_frame = build_category_summary_frame(report)

    assert report is None
    assert income_frame is None
    assert outcome_frame is None


@pytest.mark.django_db
def test_build_movement_list_xlsx_report():
    _ = MovementFactory.create_batch(10)
    xlsx_report, empty = build_movement_list_xlsx_report()

    assert empty is False


@pytest.mark.django_db
def test_build_movement_list_xlsx_report_empty():
    xlsx_report, empty = build_movement_list_xlsx_report()

    assert empty is True
