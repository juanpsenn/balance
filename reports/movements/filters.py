import django_filters

from app.models import Movement


class MovementListFilter(django_filters.FilterSet):
    class Meta:
        model = Movement
        fields = {"date_time": ["exact", "lte", "gte"]}
