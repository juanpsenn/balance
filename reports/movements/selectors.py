from typing import Optional

import pandas as pd
from django_pandas import io

from app import models
from reports.movements.filters import MovementListFilter
from reports.utils.xlsx import build_xlsx_report


def movement_list_report(*, filters: dict = None):
    filters = filters or {}
    qs = models.Movement.objects.all()

    return MovementListFilter(filters, qs).qs


def build_movement_list_base_frame(movements=None) -> Optional[pd.DataFrame]:
    column_names = [
        "Fecha",
        "Colaborador",
        "Banco",
        "Tipo",
        "Prioridad",
        "Centro de costo",
        "Motivo",
        "Descripcion",
        "Monto",
        "Saldo parcial",
        "Saldo parcial cuenta",
    ]
    fieldnames = [
        "date_time",
        "account__owner__name",
        "account__bank__name",
        "movement_type__name",
        "movement_priority__name",
        "cost_center__name",
        "category__name",
        "description",
        "amount",
        "balance_to_date",
        "balance_to_date_ac",
    ]
    if movements:
        movements = movements.select_related(
            "account__bank", "account__owner", "category"
        ).order_by("date_time", "registred")
        frame = io.read_frame(
            movements,
            fieldnames=fieldnames,
            column_names=column_names,
        )
        frame["Fecha"] = frame["Fecha"].apply(lambda x: pd.to_datetime(x).date())
        frame["Monto"] = frame["Monto"].apply(lambda x: round(float(x), 2))
        frame["Saldo parcial"] = frame["Saldo parcial"].apply(
            lambda x: round(float(x), 2)
        )
        frame["Saldo parcial cuenta"] = frame["Saldo parcial cuenta"].apply(
            lambda x: round(float(x), 2)
        )
        return frame
    return None


def build_category_summary_frame(frame: pd.DataFrame):
    income_frame, outcome_frame = None, None
    if frame is not None:
        frame["is_income"] = frame["Monto"] > 0
        income_frame = (
            frame.loc[frame["is_income"] == bool(1)]
            .groupby(["Motivo"])["Monto"]
            .sum()
            .reset_index()
        )
        outcome_frame = (
            frame.loc[frame["is_income"] == bool(0)]
            .groupby(["Motivo"])["Monto"]
            .sum()
            .reset_index()
        )

        frame.drop(
            columns=[
                "is_income",
            ],
            inplace=True,
        )
    return income_frame, outcome_frame


def build_movement_list_xlsx_report(*, filters=None):
    movements = movement_list_report(filters=filters)
    base_frame = build_movement_list_base_frame(movements)
    income_frame, outcome_frame = build_category_summary_frame(base_frame)
    reports = [
        {"df": base_frame, "sheet": "movimientos"},
        {"df": income_frame, "sheet": "Ingresos X Motivo"},
        {"df": outcome_frame, "sheet": "Egresos x Motivo"},
    ]
    return build_xlsx_report(reports)
