from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from reports.categories.selectors import category_month_year


class CategoryMonthByYearReport(APIView):
    class FilterSerializer(serializers.Serializer):
        year = serializers.DateField(input_formats=["%Y"], required=True)
        categories = serializers.ListSerializer(
            child=serializers.IntegerField(required=True)
        )

    def post(self, request):
        filter_serializer = self.FilterSerializer(data=request.data)
        filter_serializer.is_valid(raise_exception=True)

        results = category_month_year(**filter_serializer.validated_data)

        return Response(results, status=200 if results else 204)
