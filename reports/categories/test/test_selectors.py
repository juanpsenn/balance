import datetime

import pytest

from app.generics.tests.factories.generics import CategoryFactory
from app.movements.tests.factories.movement import MovementFactory
from app.utils.dates import get_timezone
from reports.categories.selectors import category_month_year


@pytest.mark.django_db
def test_category_month_year():
    category = CategoryFactory(name="ALPARGATA")

    _ = MovementFactory(
        date_time=get_timezone(date_time="2021-01-01"),
        amount=500,
        category_id=category.id,
    )
    _ = MovementFactory(
        date_time=get_timezone(date_time="2021-01-01"),
        amount=500,
        category_id=category.id,
    )

    results = category_month_year(
        year=datetime.date(year=2021, month=1, day=1), categories=[category.id]
    )

    assert len(results) == 1
    assert results[0][0]["month_number"] == 1
    assert int(results[0][0]["total_amount"]) == 1000


@pytest.mark.django_db
def test_category_month_year_empty_month():
    category = CategoryFactory(name="ALPARGATA")

    results = category_month_year(
        year=datetime.date(year=2021, month=1, day=1), categories=[category.id]
    )

    assert len(results) == 1
    assert results[0][0]["month_number"] == 1
    assert int(results[0][0]["total_amount"]) == 0
