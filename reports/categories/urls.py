from django.urls import path

from reports.categories.views import CategoryMonthByYearReport

urlpatterns = [path("category-year/", CategoryMonthByYearReport.as_view())]
