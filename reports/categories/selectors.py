from django.db.models import F
from django.db.models import Sum
from django.db.models import Value
from django.db.models.functions import ExtractMonth
from django.db.models.functions import ExtractYear

from app.models import Category
from app.movements.selectors import list_movements


def empty_categoy(*, category_id, year):
    category = (
        Category.objects.filter(pk=category_id)
        .values(category_id__name=F("name"), category_id__color=F("color"))
        .annotate(
            month_number=Value(1),
            year=Value(year.strftime("%Y")),
            total_amount=Value(0),
        )
    )
    return category


def category_month_year(*, year, categories: list = None):
    if categories is None:
        categories = []

    movements = list_movements()
    array = []

    for category_id in categories:

        result = movements.filter(date_time__year=year.year, category_id=category_id)
        result = (
            result.annotate(
                month_number=ExtractMonth("date_time"),
                year=ExtractYear("date_time"),
            )
            .values(
                "month_number",
                "year",
                "category_id__name",
                "category_id__color",
            )
            .annotate(total_amount=Sum("amount"))
            .order_by("month_number")
        )

        if result.count() > 0:
            array.append(result)
        else:
            array.append(empty_categoy(category_id=category_id, year=year))

    return array
