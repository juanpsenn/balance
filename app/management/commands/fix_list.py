from django.core.management.base import BaseCommand

from app.movements.utils import fix_linked_list


class Command(BaseCommand):
    def handle(self, *args, **options):
        fix_linked_list()
