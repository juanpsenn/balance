from django.core.management.base import BaseCommand

from app.models import Area
from app.models import MovementPriority
from app.models import MovementType
from app.movements.utils import AREAS
from app.movements.utils import MOVEMENT_PRIORITY
from app.movements.utils import MOVEMENT_TYPE


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Loading support data...")

        try:

            # CARGA DE LOS DATOS INICIALES

            for type in MOVEMENT_TYPE:
                MovementType.objects.get_or_create(name=type)

            for priority in MOVEMENT_PRIORITY:
                MovementPriority.objects.get_or_create(name=priority)

            for area in AREAS:
                Area.objects.get_or_create(name=area)

            self.stdout.write("........ GOOD ........")

        except Exception as error:
            self.stderr.write(error)
