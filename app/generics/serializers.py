from rest_framework import serializers

from app.accounts.serializers import AccountOwnerSerializer
from app.models import Area
from app.models import Bank
from app.models import Category
from app.models import MovementPriority
from app.models import MovementType
from app.models import Owner


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = "__all__"


class BankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = "__all__"


class OwnerSerializer(serializers.ModelSerializer):
    accounts = serializers.SerializerMethodField()

    def get_accounts(self, owner):
        accounts = owner.accounts.filter(is_active=True).order_by("bank__name")
        return AccountOwnerSerializer(accounts, many=True).data

    class Meta:
        model = Owner
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"

    # Se sobre escribe el metodo de repesentacion del serializer
    # para poder toma area y serializarla en los GETS
    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret["area"] = AreaSerializer(instance.area).data if instance.area else None
        ret["movement_type"] = (
            MovementTypeSerializer(instance.movement_type).data
            if instance.movement_type
            else None
        )
        ret["movement_priority"] = (
            MovementPrioritySerializer(instance.movement_priority).data
            if instance.movement_priority
            else None
        )

        return ret


class MovementPrioritySerializer(serializers.ModelSerializer):
    class Meta:
        model = MovementPriority
        fields = "__all__"


class MovementTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovementType
        fields = "__all__"
