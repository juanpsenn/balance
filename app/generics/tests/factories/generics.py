import factory
from faker import Factory

from app.models import Area
from app.models import Bank
from app.models import Category
from app.models import Owner

faker = Factory.create()


class AreaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Area

    name = faker.word()


class BankFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Bank

    name = faker.word()


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = faker.word()


class OwnerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Owner

    name = factory.LazyAttribute(lambda _: faker.name())
    document_number = faker.random_int()
