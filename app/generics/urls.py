from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from app.generics.views import AreaViewSet
from app.generics.views import BankViewSet
from app.generics.views import CategoryViewSet
from app.generics.views import ExtradataMovements
from app.generics.views import OwnerViewSet

router = DefaultRouter()
router.register("banks", BankViewSet)
router.register("categories", CategoryViewSet)
router.register("owners", OwnerViewSet)
router.register("areas", AreaViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("extradata-movements/", ExtradataMovements.as_view()),
]
