from rest_framework import views
from rest_framework import viewsets
from rest_framework.response import Response

from app.generics.serializers import AreaSerializer
from app.generics.serializers import BankSerializer
from app.generics.serializers import CategorySerializer
from app.generics.serializers import MovementPrioritySerializer
from app.generics.serializers import MovementTypeSerializer
from app.generics.serializers import OwnerSerializer
from app.models import Area
from app.models import Bank
from app.models import Category
from app.models import MovementPriority
from app.models import MovementType
from app.models import Owner


class BankViewSet(viewsets.ModelViewSet):
    queryset = Bank.objects.all().order_by("name")
    serializer_class = BankSerializer


class OwnerViewSet(viewsets.ModelViewSet):
    queryset = Owner.objects.filter(is_active=True).order_by("name")
    serializer_class = OwnerSerializer


class CategoryViewSet(viewsets.ModelViewSet):

    queryset = Category.objects.all().order_by("name")
    serializer_class = CategorySerializer


class AreaViewSet(viewsets.ModelViewSet):
    queryset = Area.objects.all().order_by("name")
    serializer_class = AreaSerializer


class ExtradataMovements(views.APIView):
    def get(self, request):
        priorities = MovementPriority.objects.all()
        types = MovementType.objects.all()
        results = {
            "priorities": MovementPrioritySerializer(priorities, many=True).data,
            "types": MovementTypeSerializer(types, many=True).data,
        }
        return Response(results, status=200)
