# Generated by Django 3.2 on 2021-06-20 23:47
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0003_category_color"),
    ]

    operations = [
        migrations.CreateModel(
            name="Group",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=128)),
                ("description", models.TextField(null=True)),
                (
                    "accounts",
                    models.ManyToManyField(related_name="groups", to="app.Account"),
                ),
            ],
        ),
    ]
