import uuid

from django.db import models


# Create your models here.
class Bank(models.Model):
    name = models.CharField(max_length=128)


class Owner(models.Model):
    name = models.CharField(max_length=128)
    document_number = models.IntegerField(null=True)
    is_active = models.BooleanField(default=True)


class MovementType(models.Model):
    name = models.CharField(max_length=128)


class MovementPriority(models.Model):
    name = models.CharField(max_length=128)


class Account(models.Model):
    owner = models.ForeignKey(
        "app.Owner",
        on_delete=models.DO_NOTHING,
        null=True,
        related_name="accounts",
    )
    bank = models.ForeignKey(
        "app.Bank",
        on_delete=models.DO_NOTHING,
        null=True,
        related_name="accounts",
    )
    balance = models.DecimalField(max_digits=15, decimal_places=5, default=0)
    user = models.ForeignKey(
        "auth.User",
        on_delete=models.DO_NOTHING,
        related_name="accounts",
        null=True,
    )
    last_update = models.DateTimeField()
    is_active = models.BooleanField(default=True)


class Area(models.Model):
    name = models.CharField(max_length=128)


class Category(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField(null=True)
    color = models.CharField(max_length=8, null=True)
    area = models.ForeignKey(
        "app.Area",
        on_delete=models.DO_NOTHING,
        related_name="categories",
        null=True,
    )
    movement_type = models.ForeignKey(
        "app.MovementType",
        on_delete=models.DO_NOTHING,
        related_name="categories",
        null=True,
    )
    movement_priority = models.ForeignKey(
        "app.MovementPriority",
        on_delete=models.DO_NOTHING,
        related_name="categories",
        null=True,
    )
    is_active = models.BooleanField(default=True)


class Movement(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    registred = models.DateTimeField(auto_now_add=True)
    aproved = models.DateTimeField(null=True)
    date_time = models.DateTimeField(null=True)
    amount = models.DecimalField(
        max_digits=15,
        decimal_places=5,
    )
    balance_to_date = models.DecimalField(
        max_digits=15,
        decimal_places=5,
    )
    balance_to_date_ac = models.DecimalField(max_digits=15, decimal_places=5, default=0)
    category = models.ForeignKey(
        "app.Category", on_delete=models.DO_NOTHING, related_name="movements"
    )
    description = models.TextField(null=True)
    account = models.ForeignKey(
        "app.Account", on_delete=models.DO_NOTHING, related_name="movements"
    )
    previous = models.OneToOneField(
        "self", on_delete=models.DO_NOTHING, null=True, related_name="next"
    )
    previous_ac = models.OneToOneField(
        "self", on_delete=models.DO_NOTHING, null=True, related_name="next_ac"
    )
    registred_by = models.ForeignKey(
        "auth.User",
        on_delete=models.DO_NOTHING,
        related_name="registered_movements",
        null=True,
    )
    approved_by = models.ForeignKey(
        "auth.User",
        on_delete=models.DO_NOTHING,
        related_name="approved_movements",
        null=True,
    )
    document = models.URLField(null=True)
    movement_type = models.ForeignKey(
        "app.MovementType",
        on_delete=models.DO_NOTHING,
        related_name="movements",
        null=True,
    )
    movement_priority = models.ForeignKey(
        "app.MovementPriority",
        on_delete=models.DO_NOTHING,
        related_name="movements",
        null=True,
    )
    cost_center = models.ForeignKey(
        "app.Area", on_delete=models.DO_NOTHING, related_name="movements", null=True
    )

    @property
    def has_next(self):
        return hasattr(self, "next") and self.next is not None

    @property
    def has_next_ac(self):
        return hasattr(self, "next_ac") and self.next_ac is not None


class Group(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField(null=True)
    accounts = models.ManyToManyField("app.Account", related_name="groups")
