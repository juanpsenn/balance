from django.urls import path

from app.accounts.views import AccountCreateApi
from app.accounts.views import AccountListApi

urlpatterns = [
    path("", AccountCreateApi.as_view()),
    path("list/", AccountListApi.as_view()),
]
