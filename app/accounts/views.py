from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from app.accounts.selectors import list_accounts
from app.accounts.serializers import AccountSerializer
from app.accounts.services import account_create


class AccountCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        owner = serializers.IntegerField()
        bank = serializers.IntegerField()
        user = serializers.IntegerField(allow_null=True)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        account = account_create(**serializer.validated_data)
        return Response(data=AccountSerializer(account).data, status=201)


class AccountListApi(APIView):
    def get(self, request):
        accounts = list_accounts()
        return Response(data=AccountSerializer(accounts, many=True).data, status=200)
