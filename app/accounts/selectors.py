from app.models import Account


def get_account(*, account: int):
    try:
        return Account.objects.get(pk=account)
    except Account.DoesNotExist:
        return None


def list_accounts():
    return Account.objects.filter(is_active=True).order_by("owner__name", "bank__name")
