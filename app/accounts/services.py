from decimal import Decimal

from django.utils import timezone

from app.accounts.selectors import get_account
from app.models import Account


def account_create(*, bank: int, owner: int, user: int) -> Account:
    return Account.objects.create(
        bank_id=bank, owner_id=owner, user_id=user, last_update=timezone.now()
    )


def update_balance(*, account: int, amount: Decimal) -> Account:
    account = get_account(account=account)
    account.balance += amount
    account.last_update = timezone.now()
    account.save()
    return account
