from rest_framework import serializers

from app.models import Account


class AccountSerializer(serializers.ModelSerializer):
    display_name = serializers.SerializerMethodField()

    @staticmethod
    def get_display_name(account):
        return f"{account.owner.name} - {account.bank.name}"

    class Meta:
        model = Account
        fields = "__all__"


class AccountOwnerSerializer(serializers.ModelSerializer):
    bank = serializers.CharField(source="bank.name")

    class Meta:
        model = Account
        fields = "__all__"
