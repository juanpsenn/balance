import factory
from django.utils import timezone
from faker import Factory

from app.generics.tests.factories.generics import BankFactory
from app.generics.tests.factories.generics import OwnerFactory
from app.models import Account
from app.users.tests.factories.user import UserFactory

faker = Factory.create()


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    owner = factory.SubFactory(OwnerFactory)
    bank = factory.SubFactory(BankFactory)
    last_update = timezone.now()
    user = factory.SubFactory(UserFactory)
