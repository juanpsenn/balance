from django.test import TestCase

from app.accounts.services import account_create
from app.accounts.services import update_balance
from app.accounts.tests.factories.account import AccountFactory
from app.generics.tests.factories.generics import BankFactory
from app.generics.tests.factories.generics import OwnerFactory
from app.models import Account
from app.users.tests.factories.user import UserFactory


class TestAccountServiceBasics(TestCase):
    def setUp(self) -> None:
        self.owner = OwnerFactory()
        self.bank = BankFactory()
        self.account = AccountFactory()
        self.user = UserFactory()

    def test_account_create(self):
        account = account_create(
            owner=self.owner.id, bank=self.bank.id, user=self.user.id
        )
        self.assertIsInstance(account, Account)

    def test_update_balance(self):
        update_balance(account=self.account.id, amount=1500)
        update_balance(account=self.account.id, amount=-500)

        self.account.refresh_from_db()
        self.assertEqual(self.account.balance, 1000)
