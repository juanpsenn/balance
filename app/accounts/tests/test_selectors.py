from django.test import TestCase

from app.accounts.selectors import get_account
from app.accounts.selectors import list_accounts
from app.accounts.tests.factories.account import AccountFactory
from app.models import Account


class TestAccountSelectorBasics(TestCase):
    def setUp(self) -> None:
        self.account = AccountFactory()

    def test_get_account(self):
        account = get_account(account=self.account.id)
        no_account = get_account(account=self.account.id + 1)

        self.assertIsInstance(account, Account)
        self.assertIsNone(no_account)

    def test_list_accounts(self):
        self.assertEqual(list_accounts().count(), 1)
