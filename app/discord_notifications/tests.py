import pytest

from .notifications import salary_payment
from app.movements.tests.factories.movement import MovementFactory


@pytest.mark.discord
@pytest.mark.django_db
def test_salary_payment_notification():
    movement = MovementFactory(
        document="https://firebasestorage.googleapis.com/v0/b/"
        "balance-management-de261.appspot.com/o/"
        "Eliana%20Garcia%2FSANTANDER%2F2021-08-20%2F"
        "45ac88ab-6272-48e2-a98a-05318b018cdb.jpeg?alt=media"
    )

    notification = salary_payment(movement)
    assert notification is None
