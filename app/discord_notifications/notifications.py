from .sender import send_message
from app.utils.urlshortener import short_url


def salary_payment(movement):
    movement.refresh_from_db()

    document_url = short_url(movement.document) if movement.document else None

    message = f"\n💰 Pago registrado: {movement.description or '---'}"
    message += "\n\t - Monto: $ {:,.2f}".format(abs(movement.amount))
    message += f"\n\t - Comprobante: {document_url or 'No registrado'}"

    return send_message(message, "salary_payment")
