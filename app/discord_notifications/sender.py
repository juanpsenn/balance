from discord import RequestsWebhookAdapter
from discord import Webhook

baseurl = "https://discord.com/api/webhooks"

urls = {
    "test": "/842380790800973864/"
    "gUUhX1o1aD2ggMtgPOUP5YX874be-av5Vk9qa0xOb1svYesEDeVpVsqjAzzp4k2tG_GR",
    "salary_payment": "/879183693049126963/"
    "uWQt6c5AxFZVJxnsqLUK8zHb0rnHpfQnOBvY3CgnPvUWihy6McLSPu5h17sEuupytRz8",
}


def send_message(data, webhook="test"):
    webhook_instance = Webhook.from_url(
        baseurl + urls[webhook], adapter=RequestsWebhookAdapter()
    )
    return webhook_instance.send(data)
