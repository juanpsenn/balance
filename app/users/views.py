from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from app.users.selectors import list_users
from app.users.serializers import UserSerializer
from app.users.services import user_register


class UserRegisterApi(APIView):
    permission_classes = [
        AllowAny,
    ]

    class InputSerializer(serializers.Serializer):
        username = serializers.CharField()
        password = serializers.CharField()
        email = serializers.EmailField(allow_null=True)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = user_register(**serializer.validated_data)
        return Response(data=UserSerializer(user).data, status=201)


class UserListApi(APIView):
    def get(self, request):
        users = list_users()
        return Response(data=UserSerializer(users, many=True).data, status=200)


class AuthApi(ObtainAuthToken):
    permission_classes = [
        AllowAny,
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        return Response({"token": token.key, "username": user.username})
