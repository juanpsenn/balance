from django.contrib.auth.models import User


def user_register(*, username: str, password: str, email: str) -> User:
    return User.objects.create_user(username=username, password=password, email=email)
