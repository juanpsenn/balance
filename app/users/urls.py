from django.urls import path

from app.users.views import AuthApi
from app.users.views import UserListApi
from app.users.views import UserRegisterApi

urlpatterns = [
    path("", UserRegisterApi.as_view()),
    path("auth/", AuthApi.as_view()),
    path("list/", UserListApi.as_view()),
]
