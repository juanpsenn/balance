from django.contrib.auth.models import User
from django.test import TestCase
from faker import Factory

from app.users.services import user_register

faker = Factory.create()


class TestUserBasics(TestCase):
    def test_user_register(self):
        user = user_register(
            username=faker.name(),
            password=faker.password(),
            email=faker.email(),
        )
        self.assertIsInstance(user, User)
