from django.test import TestCase

from app.users.selectors import list_users
from app.users.tests.factories.user import UserFactory


class TestUserBasics(TestCase):
    def setUp(self) -> None:
        pass

    def test_list_users(self):
        UserFactory()

        self.assertEqual(list_users().count(), 1)
