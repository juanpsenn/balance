from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from app.movements.selectors import list_movements
from app.movements.serializers import MovementSerializer
from app.movements.services import movement_delete
from app.movements.services import movement_register
from app.movements.services import movement_update
from app.movements.utils import get_document
from app.utils.http import CustomLimitOffsetPagination
from app.utils.http import formatted_params


class MovementRegisterApi(APIView):
    authentication_classes = [TokenAuthentication]
    parser_classes = [MultiPartParser]

    class InputSerializer(serializers.Serializer):
        amount = serializers.DecimalField(max_digits=15, decimal_places=5)
        date_time = serializers.CharField(max_length=10)
        category = serializers.IntegerField()
        description = serializers.CharField(max_length=255)
        account = serializers.IntegerField()
        movement_type = serializers.IntegerField(allow_null=True)
        movement_priority = serializers.IntegerField(allow_null=True)
        cost_center = serializers.IntegerField(allow_null=True)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        document = get_document(request)

        user = request.user

        movement = movement_register(
            **serializer.validated_data, user=user.id, document=document
        )
        return Response(data=MovementSerializer(movement).data, status=201)


class MovementDeleteApi(APIView):
    def put(self, request, muuid):
        _, _, _, _ = movement_delete(muuid=muuid)
        return Response(status=201)


class MovementUpdateApi(APIView):
    parser_classes = [MultiPartParser]

    class InputSerializer(serializers.Serializer):
        amount = serializers.DecimalField(max_digits=15, decimal_places=5)
        category = serializers.IntegerField()
        description = serializers.CharField(max_length=255)
        movement_type = serializers.IntegerField(allow_null=True)
        movement_priority = serializers.IntegerField(allow_null=True)
        cost_center = serializers.IntegerField(allow_null=True)

    def put(self, request, muuid):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        document = (
            get_document(request)
            if type(request.data.get("document")) != str
            else request.data.get("document")
        )

        movement = movement_update(
            **serializer.validated_data, muuid=muuid, document=document
        )

        return Response(data=MovementSerializer(movement).data, status=201)


class MovementListApi(APIView, CustomLimitOffsetPagination):
    def get(self, request):
        movements = list_movements(**formatted_params(request.query_params))
        paginated_movements = self.paginate_queryset(movements, request, view=self)
        return self.get_paginated_response(
            MovementSerializer(paginated_movements, many=True).data
        )
