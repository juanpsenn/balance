from django.db.models import Q

from app.models import Movement
from app.movements import querys
from app.utils.dates import get_timezone


def get_movement(*, movement: str) -> Movement:
    try:
        return Movement.objects.get(pk=movement)
    except Movement.DoesNotExist:
        return None


def get_last_from_date(*, date: str):
    return Movement.objects.filter(date_time__date=date).order_by("registred").last()


def get_first_from_date(*, date: str, account: int = None):
    q_account = querys.get_query_by_account(account)
    return (
        Movement.objects.filter(q_account, date_time__date=date)
        .order_by("registred")
        .first()
    )


def get_first_next_from_date(*, date: str, account: int = None):
    q_account = querys.get_query_by_account(account)
    return (
        Movement.objects.filter(q_account, date_time__date__gt=date)
        .order_by("date_time", "registred")
        .first()
    )


def get_most_recent_to_date(*, date: str, exclude: str = None, account: int = None):
    q_account = querys.get_query_by_account(account)
    return (
        Movement.objects.filter(q_account, date_time__date__lte=date)
        .order_by("date_time", "registred")
        .exclude(Q(id=exclude) if exclude else Q())
        .last()
    )


def list_movements(
    *,
    from_date: str = None,
    to_date: str = None,
    categories: list = None,
    description: str = None,
    accounts: list = None,
    orderby: str = "date_time"
):
    q_dates = querys.get_query_between_dates(from_date=from_date, to_date=to_date)
    q_categories = querys.get_query_by_category(categories=categories)
    q_description = querys.get_query_by_description(description=description)
    q_accounts = querys.get_query_in_accounts(accounts=accounts)
    return Movement.objects.filter(
        q_dates, q_categories, q_description, q_accounts
    ).order_by(orderby, "-registred")


def get_related_movements_for_new_movement(*, date_time, movement, account: int = None):
    prev_movement = get_most_recent_to_date(
        date=get_timezone(date_time=date_time).date(),
        exclude=movement.id,
        account=account,
    )
    if account is None:
        if prev_movement and prev_movement.has_next:
            next_movement = prev_movement.next
        else:
            next_movement = get_first_next_from_date(
                date=get_timezone(date_time=date_time).date()
            )
    else:
        if prev_movement and prev_movement.has_next_ac:
            next_movement = prev_movement.next_ac
        else:
            next_movement = get_first_next_from_date(
                date=get_timezone(date_time=date_time).date(), account=account
            )
    return next_movement, prev_movement
