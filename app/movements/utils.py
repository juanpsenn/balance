from app.models import Account
from app.models import Movement


MOVEMENT_TYPE = ["FIJO", "VARIABLE"]
MOVEMENT_PRIORITY = ["ORDINARIO", "EXTRAORDINARIO"]
AREAS = [
    "COMERCIAL",
    "ADMINISTRACION",
    "RECURSOS HUMANOS",
    "GERENCIA",
    "SISTEMAS",
    "OTROS INGRESOS",
]


def get_document(request):
    if "document" in request.FILES:
        document = request.FILES["document"]
    else:
        document = None
    return document


def fix_linked_list():
    accounts = Account.objects.all()
    for a in accounts:
        movements = Movement.objects.filter(account=a).order_by(
            "date_time", "registred"
        )
        movements.update(previous_ac=None)

        for i, m in enumerate(movements):
            if i < len(movements) - 1:
                if i == 0:
                    m.balance_to_date_ac = m.amount
                    m.save()

                movements[i + 1].balance_to_date_ac = (
                    m.balance_to_date_ac + movements[i + 1].amount
                )
                movements[i + 1].previous_ac = m
                movements[i + 1].save()
