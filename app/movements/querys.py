from django.db.models import Q


def get_query_in_accounts(*, accounts: list):
    q = Q()
    if accounts:
        q = Q(account_id__in=accounts)
    return q


def get_query_by_account(account: int = None):
    q = Q()
    if account:
        q = Q(account_id=account)
    return q


def get_query_between_dates(*, from_date, to_date):
    q = Q()
    if from_date and to_date:
        q = Q(date_time__date__range=[from_date, to_date])
    elif from_date:
        q = Q(date_time__date__gte=from_date)
    elif to_date:
        q = Q(date_time__date__lte=to_date)
    return q


def get_query_by_category(*, categories: list):
    q = Q()
    if categories:
        q = Q(category_id__in=categories)
    return q


def get_query_by_description(*, description: str):
    q = Q()
    if description and description.strip() != "":
        q = Q(description__icontains=description.strip())
    return q
