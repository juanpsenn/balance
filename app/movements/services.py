from decimal import Decimal

from django.db import transaction

from app.accounts.services import update_balance
from app.models import Movement
from app.movements.selectors import get_movement
from app.movements.selectors import get_related_movements_for_new_movement
from app.utils.dates import get_timezone
from app.utils.firebase import get_document_path
from app.utils.firebase import upload_file
from notifications.senders import NotificationSender


def movement_register(
    *,
    date_time: str,
    amount: float,
    category: int,
    description: str,
    account: int,
    user: int,
    movement_type: int = None,
    movement_priority: int = None,
    cost_center: int = None,
    document=None
) -> Movement:
    with transaction.atomic():
        movement = Movement.objects.create(
            amount=amount,
            date_time=get_timezone(date_time=date_time),
            balance_to_date=0,
            category_id=category,
            description=description,
            account_id=account,
            registred_by_id=user,
            movement_type_id=movement_type,
            movement_priority_id=movement_priority,
            cost_center_id=cost_center,
        )

        if document:
            update_document(document=document, movement=movement)

        update_balance(account=account, amount=movement.amount)

        insert_movement_into_list(date_time, movement)
        insert_movement_into_list(date_time, movement, account_related=True)

        # TODO: abstract notifications send process
        send_notification(movement, "create")
    return movement


def send_notification(movement, action):
    try:
        notifications = movement.category.notifications.filter(on_action=action)
        if notifications:
            for n in notifications:
                sender = NotificationSender(n, movement)
                sender.send_message()
    except Exception as err:
        print(err)


def insert_movement_into_list(date_time, movement, account_related: bool = False):
    next_movement, prev_movement = get_related_movements_for_new_movement(
        date_time=date_time,
        movement=movement,
        account=movement.account_id if account_related else None,
    )
    if next_movement:
        update_movement_pointer(
            from_movement=movement,
            to_movement=next_movement,
            account_related=account_related,
        )
    if prev_movement:
        update_movement_pointer(
            from_movement=prev_movement,
            to_movement=movement,
            account_related=account_related,
        )
    update_partial_balance(movement=movement)
    if account_related:
        update_partial_balance_ac_related(movement=movement)


def movement_delete(*, muuid: str):
    with transaction.atomic():
        prev_movement, _, next_movement = remove_movement(muuid)
        (
            prev_ac_movement,
            movement,
            next_ac_movement,
        ) = remove_ac_related_movement(muuid)

        update_balance(account=movement.account.id, amount=-movement.amount)

        # Delete model
        movement.refresh_from_db()
        movement.delete()

    return prev_movement, next_movement, prev_ac_movement, next_ac_movement


def remove_movement(muuid):
    prev_movement, movement, next_movement = get_related_movements(muuid=muuid)

    # Delete relation
    movement.previous = None
    movement.save()

    # Update index between
    if prev_movement and next_movement:
        update_movement_pointer(from_movement=prev_movement, to_movement=next_movement)
        # Trigger a partial balance update
        update_partial_balance(movement=prev_movement)

    elif next_movement:
        next_movement.previous = None
        next_movement.balance_to_date -= movement.amount
        next_movement.save()

        # Trigger a partial balance update
        update_partial_balance(movement=next_movement)

    return prev_movement, movement, next_movement


def remove_ac_related_movement(muuid):
    prev_ac_movement, movement, next_ac_movement = get_related_movements(
        muuid=muuid, account_related=True
    )

    # Delete relation
    movement.previous_ac = None
    movement.save()

    # Update index between
    if prev_ac_movement and next_ac_movement:
        update_movement_pointer(
            from_movement=prev_ac_movement,
            to_movement=next_ac_movement,
            account_related=True,
        )
        # Trigger a partial balance update
        update_partial_balance_ac_related(movement=prev_ac_movement)

    elif next_ac_movement:
        next_ac_movement.previous_ac = None
        next_ac_movement.balance_to_date_ac -= movement.amount
        next_ac_movement.save()

        # Trigger a partial balance update
        update_partial_balance_ac_related(movement=next_ac_movement)

    return prev_ac_movement, movement, next_ac_movement


def movement_update(
    *,
    muuid: str,
    amount: float,
    category: int,
    description: str,
    movement_type: int = None,
    movement_priority: int = None,
    cost_center: int = None,
    document=None
):
    movement = get_movement(movement=muuid)
    with transaction.atomic():
        if movement:
            if movement.amount != amount:
                update_balance(
                    account=movement.account.id,
                    amount=-movement.amount + Decimal(amount),
                )

                movement.amount = amount
                movement.save()

                update_partial_balance(movement=movement)
                update_partial_balance_ac_related(movement=movement)

            if document == "null" or not document:
                movement.document = None
            elif type(document) == str:
                movement.document = document
            elif type(document) != str:
                update_document(document=document, movement=movement)

            movement.category_id = category
            movement.description = description
            movement.movement_type_id = movement_type
            movement.movement_priority_id = movement_priority
            movement.cost_center_id = cost_center
            movement.save()

    send_notification(movement, "update")
    return movement


def update_document(*, document, movement):
    path_on_cloud = get_document_path(document=document, movement=movement)
    movement.document = upload_file(file=document, path_on_cloud=path_on_cloud)
    movement.save()


def update_movement_pointer(
    *, from_movement: Movement, to_movement: Movement, account_related: bool = False
):
    if not account_related:
        to_movement.previous = from_movement
        to_movement.save()
    else:
        to_movement.previous_ac = from_movement
        to_movement.save()


def update_partial_balance(*, movement: Movement):
    movement.refresh_from_db()
    if movement.previous:
        movement.balance_to_date = movement.previous.balance_to_date + movement.amount
    else:
        movement.balance_to_date = movement.amount
    movement.save()

    while movement.has_next:
        movement = movement.next
        movement.balance_to_date = movement.previous.balance_to_date + movement.amount
        movement.save()


def update_partial_balance_ac_related(*, movement: Movement):
    movement.refresh_from_db()
    if movement.previous_ac:
        movement.balance_to_date_ac = (
            movement.previous_ac.balance_to_date_ac + movement.amount
        )
    else:
        movement.balance_to_date_ac = movement.amount
    movement.save()

    while movement.has_next_ac:
        movement = movement.next_ac
        movement.balance_to_date_ac = (
            movement.previous_ac.balance_to_date_ac + movement.amount
        )
        movement.save()


def get_related_movements(*, muuid: str, account_related: bool = False):
    prev_movement, next_movement = None, None
    movement = get_movement(movement=muuid)
    if not account_related:
        if movement.previous:
            prev_movement = movement.previous
        if movement.has_next:
            next_movement = movement.next
    else:
        if movement.previous_ac:
            prev_movement = movement.previous_ac
        if movement.has_next_ac:
            next_movement = movement.next_ac
    return prev_movement, movement, next_movement
