from faker import Factory

from app.accounts.tests.factories.account import AccountFactory
from app.generics.tests.factories.generics import CategoryFactory
from app.movements.services import movement_register

faker = Factory.create()


def setup_test_account_movement(*, n_movements: int):
    movements = []
    account = AccountFactory()
    for _ in range(n_movements):
        movement = movement_register(
            date_time="2000-01-01",
            amount=10,
            category=CategoryFactory().id,
            description=faker.text(),
            account=account.id,
            user=account.user.id,
        )
        movements.append(movement)
    return account, movements
