import uuid
from datetime import timedelta

from django.test import TestCase

from app.accounts.tests.factories.account import AccountFactory
from app.generics.tests.factories.generics import CategoryFactory
from app.models import Movement
from app.movements.selectors import get_first_from_date
from app.movements.selectors import get_last_from_date
from app.movements.selectors import get_most_recent_to_date
from app.movements.selectors import get_movement
from app.movements.selectors import list_movements
from app.movements.services import get_related_movements
from app.movements.tests.factories.movement import MovementFactory
from app.movements.tests.utils import setup_test_account_movement
from app.utils.dates import get_timezone


class TestMovementSelectorBasics(TestCase):
    def setUp(self) -> None:
        self.test_date = get_timezone(date_time="2021-01-05")
        self.movements = MovementFactory.create_batch(5, date_time=self.test_date)

    def test_get_movement(self):
        movement = get_movement(movement=self.movements[0].id)
        no_movement = get_movement(movement=uuid.uuid4().hex)

        self.assertIsInstance(movement, Movement)
        self.assertIsNone(no_movement)

    def test_get_last_from_date(self):
        movement = get_last_from_date(date=self.test_date.date())

        self.assertEqual(movement, self.movements[-1])

    def test_get_first_from_date(self):
        movement = get_first_from_date(date=self.test_date.date())

        self.assertEqual(movement, self.movements[0])

    def test_get_most_recent_to_date(self):
        fisrt_movement = MovementFactory(date_time=self.test_date + timedelta(days=1))
        _ = MovementFactory(date_time=self.test_date + timedelta(days=3))

        movement = get_most_recent_to_date(date=self.test_date + timedelta(days=2))

        self.assertEqual(movement, fisrt_movement)

    def test_get_by_category(self):
        category = CategoryFactory(name="Test category")
        _ = MovementFactory(category=category)

        movements = list_movements(categories=[category.id])

        self.assertEqual(movements.count(), 1)

    def test_get_by_description(self):
        _ = MovementFactory(description="A test description")

        movements = list_movements(description="test description")

        self.assertEqual(movements.count(), 1)

    def test_get_by_account(self):
        account = AccountFactory.create()
        _ = MovementFactory(account=account)

        movements = list_movements(accounts=[account.id])

        self.assertEqual(movements.count(), 1)

    def test_get_related_ac_movements(self):
        account, movements = setup_test_account_movement(n_movements=3)
        prev_movement, movement, next_movement = get_related_movements(
            muuid=movements[1].id, account_related=True
        )

        assert next_movement.previous_ac == movement
        assert prev_movement.next_ac == movement

    def test_get_related_movements(self):
        account, movements = setup_test_account_movement(n_movements=3)
        prev_movement, movement, next_movement = get_related_movements(
            muuid=movements[1].id
        )

        assert next_movement.previous == movement
        assert prev_movement.next == movement
