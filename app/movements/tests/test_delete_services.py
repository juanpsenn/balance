import pytest

from app.movements.services import movement_delete
from app.movements.tests.utils import setup_test_account_movement


@pytest.mark.django_db
def test_movement_delete():
    account, movements = setup_test_account_movement(n_movements=3)

    movements = [
        prev_movement,
        next_movement,
        prev_ac_movement,
        next_ac_movement,
    ] = movement_delete(muuid=movements[1].id)

    account.refresh_from_db()

    for m in movements:
        m.refresh_from_db()

    assert prev_movement == next_movement.previous
    assert prev_movement.next == next_movement

    assert prev_ac_movement == next_ac_movement.previous_ac
    assert prev_ac_movement.next_ac == next_ac_movement

    assert prev_movement.balance_to_date == 10
    assert next_movement.balance_to_date == 20
    assert account.balance == 20


@pytest.mark.django_db
def test_movement_delete_first():
    account, movements = setup_test_account_movement(n_movements=3)

    prevm, _, prevm_ac, _ = movement_delete(muuid=movements[0].id)

    [m.refresh_from_db() for m in movements[1:]]
    account.refresh_from_db()

    assert prevm is None
    assert prevm_ac is None
    assert movements[1].balance_to_date == 10
    assert movements[2].balance_to_date == 20

    assert movements[2].balance_to_date_ac == 20

    assert account.balance == 20


@pytest.mark.django_db
def test_movement_delete_last():
    account, movements = setup_test_account_movement(n_movements=3)

    movement_delete(muuid=movements[2].id)

    [m.refresh_from_db() for m in movements[:2]]
    account.refresh_from_db()

    assert movements[0].balance_to_date == 10
    assert movements[1].balance_to_date == 20

    assert movements[1].balance_to_date_ac == 20

    assert movements[1].has_next is False
    assert movements[1].has_next_ac is False

    assert account.balance == 20
