import factory
from django.utils import timezone
from faker import Factory

from app.accounts.tests.factories.account import AccountFactory
from app.generics.tests.factories.generics import CategoryFactory
from app.models import Movement
from app.users.tests.factories.user import UserFactory

faker = Factory.create()


class MovementFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Movement

    amount = faker.random_number()
    balance_to_date = amount
    date_time = timezone.now()
    category = factory.SubFactory(CategoryFactory)
    account = factory.SubFactory(AccountFactory)
    registred_by = factory.SubFactory(UserFactory)
