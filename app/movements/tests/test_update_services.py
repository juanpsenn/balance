import pytest

from app.generics.tests.factories.generics import CategoryFactory
from app.models import Account
from app.movements.services import movement_update
from app.movements.tests.utils import setup_test_account_movement
from app.movements.utils import fix_linked_list


@pytest.mark.django_db
def test_movement_update():
    account, movements = setup_test_account_movement(n_movements=3)
    category = CategoryFactory(name="Update")

    movement_update(
        muuid=movements[1].id,
        amount=20,
        category=category.id,
        description="Update",
    )

    [m.refresh_from_db() for m in movements]
    account.refresh_from_db()

    assert movements[1].balance_to_date == 30
    assert movements[2].balance_to_date == 40
    assert movements[1].category == category
    assert movements[1].description == "Update"

    assert movements[1].balance_to_date_ac == 30

    assert account.balance == 40


@pytest.mark.django_db
def test_fix_linked_list():
    def assert_fix(mov_list, acc):
        assert mov_list[0].previous_ac is None
        assert mov_list[1].previous_ac == mov_list[0]
        assert mov_list[2].previous_ac == mov_list[1]
        assert mov_list[-1].balance_to_date_ac == acc.balance
        assert mov_list[-1].has_next_ac is False

    m1 = [account, movements] = setup_test_account_movement(n_movements=10)
    m2 = [account_2, movements_2] = setup_test_account_movement(n_movements=10)

    fix_linked_list()

    for m in m1 + m2:
        if isinstance(m, list):
            for i in m:
                i.refresh_from_db()
        else:
            m.refresh_from_db()

    assert Account.objects.count() == 2

    assert_fix(movements, account)
    assert_fix(movements_2, account_2)
