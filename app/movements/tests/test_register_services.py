from datetime import timedelta

from django.test import TestCase
from faker import Factory

from app.accounts.tests.factories.account import AccountFactory
from app.generics.tests.factories.generics import CategoryFactory
from app.models import Movement
from app.movements.services import movement_register
from app.movements.services import update_movement_pointer
from app.movements.tests.factories.movement import MovementFactory
from app.users.tests.factories.user import UserFactory
from app.utils.dates import get_timezone

faker = Factory.create()


class TestMovementBasics(TestCase):
    def setUp(self) -> None:
        self.account = AccountFactory()
        self.category = CategoryFactory()
        self.user = UserFactory()

    def test_movement_register(self):
        test_date = get_timezone(date_time="2021-01-01")
        mov_a = MovementFactory(date_time=test_date, amount=10, balance_to_date=10)
        mov_b = MovementFactory(
            date_time=test_date + timedelta(days=2), previous=mov_a, amount=10
        )

        movement = movement_register(
            date_time="2021-01-02",
            amount=10,
            category=self.category.id,
            description=faker.text(),
            account=self.account.id,
            user=self.user.id,
        )

        next_movement = movement_register(
            date_time="2021-01-02",
            amount=10,
            category=self.category.id,
            description=faker.text(),
            account=self.account.id,
            user=self.user.id,
        )

        mov_a.refresh_from_db()
        mov_b.refresh_from_db()
        movement.refresh_from_db()
        next_movement.refresh_from_db()
        self.account.refresh_from_db()

        self.assertIsInstance(movement, Movement)
        self.assertEqual(20, self.account.balance)

        self.assertEqual(movement.previous, mov_a)
        self.assertEqual(mov_b.previous, next_movement)

        self.assertEqual(mov_a.balance_to_date, 10)
        self.assertEqual(movement.balance_to_date, 20)
        self.assertEqual(next_movement.balance_to_date, 30)
        self.assertEqual(mov_b.balance_to_date, 40)
        # TODO: test account related linked list integrity

    def test_movement_register_with_different_accounts(self):
        acc_a = AccountFactory()
        acc_b = AccountFactory()
        acc_a_movements = [
            movement_register(
                date_time="2021-01-02",
                amount=10,
                category=self.category.id,
                description=faker.text(),
                account=acc_a.id,
                user=self.user.id,
            )
            for _ in range(2)
        ]

        acc_b_movements = [
            movement_register(
                date_time="2021-01-02",
                amount=5,
                category=self.category.id,
                description=faker.text(),
                account=acc_b.id,
                user=self.user.id,
            )
            for _ in range(2)
        ]

        assert acc_b_movements[0].previous == acc_a_movements[1]
        assert acc_b_movements[0].previous_ac is None
        assert acc_b_movements[1].previous_ac == acc_b_movements[0]
        assert acc_b_movements[1].balance_to_date_ac == 10
        assert acc_a_movements[1].balance_to_date_ac == 20

    def test_movement_register_in_one_len_list(self):
        test_date = get_timezone(date_time="2021-01-01")
        mov_a = MovementFactory(date_time=test_date, amount=10, balance_to_date=10)

        movement = movement_register(
            date_time="2021-01-02",
            amount=10,
            category=self.category.id,
            description=faker.text(),
            account=self.account.id,
            user=self.user.id,
        )

        mov_a.refresh_from_db()
        movement.refresh_from_db()

        self.assertEqual(movement.previous, mov_a)

        self.assertEqual(mov_a.balance_to_date, 10)
        self.assertEqual(movement.balance_to_date, 20)
        # TODO: test account related linked list integrity

    def test_movement_register_in_empty_list(self):
        movement = movement_register(
            date_time="2021-01-02",
            amount=10,
            category=self.category.id,
            description=faker.text(),
            account=self.account.id,
            user=self.user.id,
        )

        movement.refresh_from_db()

        self.assertIsInstance(movement, Movement)
        self.assertEqual(movement.balance_to_date, 10)
        # TODO: test account related linked list integrity

    def test_movement_register_days_ago(self):
        # TODO: test with unsequenced list
        #  eg. 20-04 then 18-04 with no movements in 19-04
        pass

    def test_update_movement_pointer(self):
        movements = MovementFactory.create_batch(2)

        update_movement_pointer(from_movement=movements[0], to_movement=movements[1])
        update_movement_pointer(
            from_movement=movements[0],
            to_movement=movements[1],
            account_related=True,
        )

        [m.refresh_from_db() for m in movements]

        self.assertEqual(movements[0].next, movements[1])
        self.assertEqual(movements[1].previous, movements[0])

        self.assertEqual(movements[0].next_ac, movements[1])
        self.assertEqual(movements[1].previous_ac, movements[0])
