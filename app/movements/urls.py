from django.urls import path

from app.movements.views import MovementDeleteApi
from app.movements.views import MovementListApi
from app.movements.views import MovementRegisterApi
from app.movements.views import MovementUpdateApi

urlpatterns = [
    path("", MovementRegisterApi.as_view()),
    path("list/", MovementListApi.as_view()),
    path("delete/<str:muuid>/", MovementDeleteApi.as_view()),
    path("update/<str:muuid>/", MovementUpdateApi.as_view()),
]
