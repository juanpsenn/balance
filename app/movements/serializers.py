from rest_framework import serializers

from app.accounts.serializers import AccountSerializer
from app.generics.serializers import CategorySerializer
from app.models import Movement


class MovementSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    account = AccountSerializer()

    class Meta:
        model = Movement
        fields = "__all__"
