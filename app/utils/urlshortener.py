import json

import requests

headers = {
    "Content-type": "application/json",
    "apikey": "34c4daff98934f9db834360a4ca3a8a2",
    "workspace": "478c7b41ba6649289ddc7e29f684ed18",
}


def short_url(url):
    data = {"destination": url, "domain": {"fullName": "rebrand.ly"}}

    r = requests.post(
        "https://api.rebrandly.com/v1/links",
        data=json.dumps(data),
        headers=headers,
    )

    if r.status_code == requests.codes.ok:
        link = r.json()
        return f'https://{link["shortUrl"]}'
    return None
