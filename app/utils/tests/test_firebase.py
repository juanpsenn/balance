import pytest

from app.movements.tests.factories.movement import MovementFactory
from app.utils.dates import get_timezone
from app.utils.firebase import get_document_path
from app.utils.firebase import get_filename


@pytest.mark.django_db
def test_get_filename_from_document():
    movement = MovementFactory(
        document="https://firebasestorage.googleapis.com/v0/b/"
        "balance-management-de261.appspot.com/o/"
        "Mickaela%20Crespo%2FMERCADO%20PAGO%2F2021-07-05%2F"
        "24d9441d-89d8-4962-b000-13d1c178a279.png?alt=media"
    )
    filename = get_filename(movement.document)
    assert filename == "24d9441d-89d8-4962-b000-13d1c178a279.png"


@pytest.mark.django_db
def test_get_path_on_cloud():
    movement = MovementFactory(
        account__owner__name="Mickaela Crespo",
        account__bank__name="MERCADO PAGO",
        date_time=get_timezone(date_time="2021-07-05"),
        document="https://firebasestorage.googleapis.com/v0/b/"
        "balance-management-de261.appspot.com/o/"
        "Mickaela%20Crespo%2FMERCADO%20PAGO%2F2021-07-05%2F"
        "test-24d9441d-89d8-4962-b000-13d1c178a279.png?alt=media",
    )
    path = get_document_path(movement=movement)
    assert (
        path == "test/"
        "Mickaela Crespo/"
        "MERCADO PAGO/"
        "2021-07-05/"
        "24d9441d-89d8-4962-b000-13d1c178a279.png"
    )
