from django.http import QueryDict
from django.test import TestCase

from app.utils.dates import get_timezone
from app.utils.http import formatted_params
from app.utils.http import reserved


class TestUtilDates(TestCase):
    def test_get_timezone_none(self):
        self.assertIsNone(get_timezone(date_time=None))
        self.assertIsNone(get_timezone(date_time=""))


class TestUtilHttp(TestCase):
    def setUp(self) -> None:
        self.params = {key: "0" for key in reserved}

    def test_formatted_params_exclude_reserved(self):
        assert {} == formatted_params(self.params)

    def test_formatted_params_with_lists(self):
        params = QueryDict("", mutable=True)
        params.setlist("categories[]", ["1", "2"])
        assert {"categories": ["1", "2"]} == formatted_params(params)
