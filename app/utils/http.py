from rest_framework.pagination import LimitOffsetPagination

reserved = ["page", "size", "limit", "offset"]


class CustomLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 15
    max_limit = 100


def formatted_params(params):
    return {
        key.replace("[]", ""): params.get(key)
        if "[]" not in key
        else params.getlist(key)
        for key in params
        if params.get(key) != "" and key not in reserved
    }
