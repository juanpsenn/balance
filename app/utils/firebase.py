import os
import re

import pyrebase

from app.config.firebase import config
from app.models import Movement


def upload_file(*, file, path_on_cloud):
    storage = firebase_connect()
    storage.child(path_on_cloud).put(file)
    return storage.child(path_on_cloud).get_url(token=None)


def firebase_connect():
    firebase = pyrebase.initialize_app(config)
    storage = firebase.storage()
    return storage


def get_document_path(*, document: str = None, movement: Movement):
    dev_env = os.getenv("ENV") == "development"
    path = ""
    if document:
        document_name = f"{movement.id}.{doc_ext(document)}"
    else:
        document_name = f"{get_filename(movement.document)}"

    if dev_env:
        path = "test/"

    return path + (
        f"{movement.account.owner.name}/"
        f"{movement.account.bank.name}/"
        f"{str(movement.date_time.date())}/"
        f"{document_name}"
    )


def get_filename(document: str):
    try:
        return re.findall(
            r"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}.*\?alt=media",
            document.lower(),
        )[0].replace("?alt=media", "")
    except IndexError:
        return ""


def doc_ext(document):
    return str(document).split(".")[-1]
