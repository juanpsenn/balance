import datetime

from django.utils.timezone import make_aware


def get_timezone(*, date_time: str, fmt: str = None):
    if date_time:
        if fmt is None:
            fmt = "%Y-%m-%d"

        date_time = datetime.datetime.strptime(date_time, fmt)
        return make_aware(date_time)
    return None
