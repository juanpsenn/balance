from django.urls import path

from app.groups.views import GroupCreateApi
from app.groups.views import GroupListApi
from app.groups.views import GroupUpdateApi

urlpatterns = [
    path("list/", GroupListApi.as_view()),
    path("create/", GroupCreateApi.as_view()),
    path("update/<int:group>/", GroupUpdateApi.as_view()),
]
