from rest_framework import serializers

from app.accounts.serializers import AccountSerializer
from app.groups.selectors import get_group_balance
from app.models import Group


class GroupSerializer(serializers.ModelSerializer):
    group_balance = serializers.SerializerMethodField()
    accounts = serializers.SerializerMethodField()

    def get_group_balance(self, group):
        return get_group_balance(group=group)

    def get_accounts(self, instance):
        acc = instance.accounts.filter(is_active=True)
        return AccountSerializer(acc, many=True).data

    class Meta:
        model = Group
        fields = "__all__"
