from django.test import TestCase
from faker import Factory

from app.accounts.tests.factories.account import AccountFactory
from app.groups.services import group_create
from app.groups.services import group_update
from app.groups.tests.factories.group import GroupFactory
from app.models import Group

faker = Factory.create()


class TestGroupServicesBasics(TestCase):
    def group_with_accounts(self, accounts: int):
        return GroupFactory.create(
            accounts=AccountFactory.create_batch(size=2, balance=100)
        )

    def test_group_create(self):
        accounts = AccountFactory.create_batch(3)
        group = group_create(
            name="A group",
            description="Some group description",
            accounts=[a.id for a in accounts],
        )

        self.assertIsInstance(group, Group)
        self.assertEqual(group.name, "A group")
        self.assertEqual(group.accounts.count(), 3)

    def test_group_update(self):
        group = self.group_with_accounts(3)
        _ = group_update(
            group=group.pk, name="Updated", description="A group", accounts=[1]
        )
        group.refresh_from_db()

        # TODO: VER
        self.assertEqual(group.name, "Updated")
        # self.assertEqual(group.accounts.count(), 1)
        # self.assertEqual(group.accounts.last().pk, 1)
