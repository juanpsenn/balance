import factory
from faker import Factory

from app.models import Group

faker = Factory.create()


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group

    name = factory.LazyAttribute(lambda _: faker.name())

    @factory.post_generation
    def accounts(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for account in extracted:
                self.accounts.add(account)
