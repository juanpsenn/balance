from django.test import TestCase

from app.accounts.tests.factories.account import AccountFactory
from app.groups.selectors import get_group_balance
from app.groups.selectors import list_groups
from app.groups.tests.factories.group import GroupFactory


class TestGroupSelectoBasics(TestCase):
    def group_with_accounts(self):
        return GroupFactory.create(
            accounts=AccountFactory.create_batch(size=3, balance=100)
        )

    def test_list_group(self):
        group = self.group_with_accounts()
        self.assertEqual(1, list_groups().count())
        self.assertEqual(group.accounts.count(), 3)

    def test_get_group_balance(self):
        group = self.group_with_accounts()
        group_balance = get_group_balance(group=group)
        self.assertEqual(group_balance, 300)
