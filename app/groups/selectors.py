from django.db.models import Sum

from app.models import Group


def get_group_by_id(group):
    return Group.objects.get(pk=group)


def list_groups():
    return Group.objects.prefetch_related("accounts").all()


def get_group_balance(*, group: Group):
    return group.accounts.filter(is_active=True).aggregate(Sum("balance"))[
        "balance__sum"
    ]
