from app.groups.selectors import get_group_by_id
from app.models import Group


def group_create(*, name: str, description: str, accounts: list) -> Group:
    group = Group.objects.create(name=name, description=description)
    group.accounts.set(accounts)
    return group


def group_update(*, group: int, name: str, description: str, accounts: list):
    group = get_group_by_id(group)

    group.name = name
    group.description = description
    group.save()

    group.accounts.set(accounts)
    return group
