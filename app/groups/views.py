from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from app.groups.selectors import list_groups
from app.groups.serializers import GroupSerializer
from app.groups.services import group_create
from app.groups.services import group_update


class GroupListApi(APIView):
    def get(self, request):
        groups = list_groups()
        return Response(GroupSerializer(groups, many=True).data, status=200)


class GroupCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=128)
        description = serializers.CharField(max_length=255)
        accounts = serializers.ListField()

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        group = group_create(**serializer.validated_data)
        return Response(GroupSerializer(group).data, status=201)


class GroupUpdateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=128)
        description = serializers.CharField(max_length=255)
        accounts = serializers.ListField()

    def put(self, request, group):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        group = group_update(**serializer.validated_data, group=group)

        return Response(GroupSerializer(group).data, status=201)
