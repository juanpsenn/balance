from django.db import models


# Create your models here.
class TemplateNotification(models.Model):
    message = models.TextField()
    category = models.ForeignKey(
        "app.Category", on_delete=models.DO_NOTHING, related_name="notifications"
    )
    on_action = models.CharField(max_length=32)
    attr_name = models.CharField(max_length=32)
    sender_class = models.CharField(max_length=128)
    connection_string = models.TextField()
