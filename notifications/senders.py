import smtplib
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from discord import RequestsWebhookAdapter
from discord import Webhook

from notifications.models import TemplateNotification


class NotificationSender:
    def __init__(self, template: TemplateNotification, model):
        self.sender = getattr(sys.modules[__name__], template.sender_class)
        self.attr_name = template.attr_name
        self.message = template.message
        self.connection_string = template.connection_string
        self.model = model

    def get_formatted_message(self):
        return (
            self.message.replace("\\n", "\n")
            .replace("\\t", "\t")
            .format(**{self.attr_name: self.model})
            .replace("None", "---")
        )

    def send_message(self):
        message = self.get_formatted_message()
        sender = self.sender(message, self.connection_string)
        sender.send_message()


class DiscordSender:
    def __init__(self, message: str, webhook: str):
        self.message = message
        self.webhook = webhook

    def send_message(self):
        webhook_instance = Webhook.from_url(
            self.webhook, adapter=RequestsWebhookAdapter()
        )
        return webhook_instance.send(self.message)


class GmailSender:
    def __init__(self, message: str, connection: str):
        self.message = message
        self.msg_from = connection.split(":")[0]
        self.msg_to = connection.split(":")[1]
        self.password = connection.split(":")[2]

    def send_message(self):
        message = MIMEMultipart()
        message["From"] = self.msg_from
        message["To"] = self.msg_to
        message["Subject"] = "AITBalance Notification"
        message.attach(MIMEText(self.message, "plain"))

        with smtplib.SMTP("smtp.gmail.com", 587) as session:
            session.starttls()
            session.login(self.msg_from, self.password)
            session.sendmail(self.msg_from, self.msg_to, message.as_string())
