FROM python:3.8-alpine
WORKDIR /balance
COPY requirements.txt ./

RUN apk add python3-dev mariadb-dev	build-base
RUN pip install -r ./requirements.txt

COPY . .

CMD python3 manage.py runserver 0.0.0.0:8000
