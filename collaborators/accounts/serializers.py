from rest_framework import serializers

from collaborators.models import Account


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Account


class SimpleAccountSerializer(serializers.ModelSerializer):
    bank = serializers.SerializerMethodField()
    owner = serializers.SerializerMethodField()

    class Meta:
        fields = "__all__"
        model = Account

    def get_bank(self, account):
        return account.bank.name

    def get_owner(self, account):
        return account.owner.name
