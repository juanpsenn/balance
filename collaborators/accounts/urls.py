from django.urls import path

from .views import AccountCreateApi
from .views import AccountListApi

urlpatterns = [
    path("list/", AccountListApi.as_view()),
    path("create/", AccountCreateApi.as_view()),
]
