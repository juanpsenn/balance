from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import AccountSerializer
from .serializers import SimpleAccountSerializer
from .services import create_account
from .services import update_account
from collaborators.models import Account


class AccountListApi(APIView):
    def get(self, request):
        accounts = Account.objects.all()
        return Response(
            data=SimpleAccountSerializer(accounts, many=True).data,
            status=200 if accounts else 204,
        )


class AccountCreateApi(APIView):
    class InputSerilizer(serializers.Serializer):
        owner = serializers.IntegerField()
        bank = serializers.IntegerField()
        alias = serializers.CharField(allow_blank=True, max_length=128)
        cvu = serializers.CharField(allow_blank=True, max_length=128)
        account_number = serializers.CharField(max_length=128, allow_blank=True)

    def post(self, request):
        serializer = self.InputSerilizer(data=request.data)
        serializer.is_valid(raise_exception=True)

        account = create_account(**serializer.validated_data)

        return Response(AccountSerializer(account).data, status=201)


class AccountUpdateApi(APIView):
    class InputSerilizer(serializers.Serializer):
        owner = serializers.IntegerField()
        bank = serializers.IntegerField()
        alias = serializers.CharField(allow_blank=True, max_length=128)
        cvu = serializers.CharField(allow_blank=True, max_length=128)
        account_number = serializers.CharField(max_length=128, allow_blank=True)

    def put(self, request, account_id):
        serializer = self.InputSerilizer(data=request.data)
        serializer.is_valid()

        account = update_account(**serializer.validated_data, account_id=account_id)

        return (
            Response(AccountSerializer(data=account).data, status=201)
            if account
            else Response({"error": "id does not exist"}, status=400)
        )
