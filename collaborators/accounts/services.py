from collaborators.models import Account


def create_account(
    *, owner: int, bank: int, alias: str, account_number: str, cvu: str
) -> Account:
    return Account.objects.create(
        owner_id=owner,
        bank_id=bank,
        alias=alias,
        account_number=account_number,
        cvu=cvu,
    )


def update_account(
    *, account_id: int, owner: int, bank: int, alias: str, account_number: str, cvu: str
) -> Account:
    try:
        account = Account.objects.get(pk=account_id)
        account.owner_id = owner
        account.bank_id = bank
        account.alias = alias
        account.account_number = account_number
        account.cvu = cvu
        return account
    except Account.DoesNotExist:
        return None
