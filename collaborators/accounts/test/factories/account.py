import factory
from faker import Factory

from app.generics.tests.factories.generics import BankFactory
from collaborators.collaborators.test.factories.collaborator import (
    CollaboratorFactory,
)
from collaborators.models import Account

faker = Factory.create()


class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    owner = factory.SubFactory(CollaboratorFactory)
    bank = factory.SubFactory(BankFactory)
    cvu = faker.random_number(fix_len=22)
    alias = faker.name()
    account_number = faker.bothify(text="###/#####")
