from django.test import TestCase
from faker import Factory

from app.generics.tests.factories.generics import BankFactory
from collaborators.accounts.services import create_account
from collaborators.accounts.services import update_account
from collaborators.accounts.test.factories.account import AccountFactory
from collaborators.collaborators.test.factories.collaborator import (
    CollaboratorFactory,
)
from collaborators.models import Account

faker = Factory.create()


class TestAccountServices(TestCase):
    def test_create_account(self):
        owner = CollaboratorFactory()
        bank = BankFactory()
        account = create_account(
            owner=owner.id,
            bank=bank.id,
            cvu=faker.random_number(fix_len=22),
            alias=faker.name(),
            account_number=faker.bothify(text="###/#####"),
        )

        self.assertIsInstance(account, Account)

    def test_update_account(self):
        account = AccountFactory()
        bank_update = BankFactory()

        updated_account = update_account(
            account_id=account.id,
            owner=account.owner.id,
            alias=account.alias,
            account_number=account.account_number,
            cvu=account.cvu,
            bank=bank_update.id,
        )

        self.assertIsInstance(updated_account, Account)
        self.assertEqual(updated_account.bank.id, bank_update.id)
