from django.test import TestCase

from collaborators.accounts.selectors import list_accounts
from collaborators.accounts.test.factories.account import AccountFactory


class TestAccountSelectorBasics(TestCase):
    def setUp(self):
        self.account = AccountFactory()

    def test_list_accounts(self):
        self.assertEqual(list_accounts().count(), 1)
