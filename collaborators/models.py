from django.db import models

from app.models import Bank


# Create your models here.
class Collaborator(models.Model):
    name = models.CharField(max_length=128)
    salary = models.CharField(max_length=128, null=True)
    email = models.EmailField(max_length=254, null=True)
    contact_phone = models.CharField(max_length=128, null=True)
    emergency_contact = models.CharField(max_length=128, null=True)
    birthday = models.DateTimeField(null=True)
    gender = models.CharField(max_length=128, null=True)
    is_active = models.BooleanField(default=True)


class Account(models.Model):
    owner = models.ForeignKey(
        Collaborator,
        on_delete=models.DO_NOTHING,
        null=True,
        related_name="accounts",
    )
    bank = models.ForeignKey(
        Bank,
        on_delete=models.DO_NOTHING,
        null=True,
        related_name="collaborators_accounts",
    )
    cvu = models.CharField(max_length=128, null=True)
    alias = models.CharField(max_length=128, null=True)
    account_number = models.CharField(max_length=128, null=True)
