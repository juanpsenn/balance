from rest_framework import serializers

from collaborators.accounts.serializers import SimpleAccountSerializer
from collaborators.models import Collaborator


class CollaboratorSerializer(serializers.ModelSerializer):
    accounts = SimpleAccountSerializer(many=True, read_only=True)

    class Meta:
        fields = "__all__"
        model = Collaborator
