from django.test import TestCase

from collaborators.collaborators.selectors import collaborator_get
from collaborators.collaborators.selectors import collaborators_list
from collaborators.collaborators.test.factories.collaborator import (
    CollaboratorFactory,
)
from collaborators.models import Collaborator


class TestCollaboratorSelectorBasics(TestCase):
    def setUp(self) -> None:
        self.collaborator = CollaboratorFactory()

    def test_collaborator_get(self):
        # Test collaborator exist
        collaborator = collaborator_get(collaborator_id=self.collaborator.id)
        # Test collaborator does not exist
        no_collaborator = collaborator_get(collaborator_id=self.collaborator.id + 2)

        # Verify collaborator exist
        self.assertIsInstance(collaborator, Collaborator)

        # Verify collaborator does not exist
        self.assertIsNone(no_collaborator)

    def test_collaborator(self):
        # Test collaborators list count()
        self.assertEqual(collaborators_list().count(), 1)
