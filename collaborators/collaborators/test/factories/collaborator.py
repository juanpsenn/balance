from datetime import datetime

import factory
from faker import Factory

from collaborators.models import Collaborator

faker = Factory.create()


class CollaboratorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Collaborator

    name = factory.lazy_attribute(lambda _: faker.name())
    salary = factory.lazy_attribute(lambda _: faker.random_number())
    email = factory.lazy_attribute(lambda _: faker.email())
    contact_phone = factory.LazyAttribute(lambda _: faker.phone_number())
    emergency_contact = factory.LazyAttribute(lambda _: faker.phone_number())
    birthday = datetime.now()
    gender = factory.lazy_attribute(lambda _: faker.name())
