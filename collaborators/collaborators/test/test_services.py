from django.test import TestCase
from faker import Factory

from collaborators.collaborators.services import create_collaborator
from collaborators.collaborators.services import update_collaborator
from collaborators.collaborators.test.factories.collaborator import (
    CollaboratorFactory,
)
from collaborators.models import Collaborator


faker = Factory.create()


class TestCollaboratorServices(TestCase):
    def test_create_collaborator(self):
        collaborator = create_collaborator(
            name=faker.name(),
            salary=faker.random_number(),
            email=faker.email(),
            contact_phone=faker.phone_number(),
            emergency_contact=faker.phone_number(),
            birthday=faker.date_time(),
            gender=faker.name(),
        )

        self.assertIsInstance(collaborator, Collaborator)

    def test_update_collaborator(self):

        collaborator = CollaboratorFactory()

        updated_name = faker.name()
        updated_contact_phone = faker.phone_number()
        updated_salary = faker.random_number()
        updated_emergency_contact = faker.phone_number()
        updated_birthday = faker.date_time()
        updated_gender = faker.name()
        updated_email = faker.email()

        updated_collaborator = update_collaborator(
            collaborator_id=collaborator.id,
            name=updated_name,
            contact_phone=updated_contact_phone,
            email=updated_email,
            gender=updated_gender,
            birthday=updated_birthday,
            salary=updated_salary,
            emergency_contact=updated_emergency_contact,
        )

        self.assertEqual(updated_collaborator.id, collaborator.id)
        assert updated_collaborator.name != collaborator.name
        assert updated_collaborator.contact_phone != collaborator.contact_phone
        assert updated_collaborator.email != collaborator.email
        assert updated_collaborator.gender != collaborator.gender
        assert updated_collaborator.birthday != collaborator.birthday
        assert updated_collaborator.salary != collaborator.salary
        assert updated_collaborator.emergency_contact != collaborator.emergency_contact
