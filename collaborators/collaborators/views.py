from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from collaborators.accounts.serializers import SimpleAccountSerializer
from collaborators.collaborators.selectors import collaborator_get
from collaborators.collaborators.selectors import collaborators_list
from collaborators.collaborators.serializers import CollaboratorSerializer
from collaborators.collaborators.services import create_collaborator
from collaborators.collaborators.services import update_collaborator
from collaborators.models import Collaborator


class CollaboratorsListApi(APIView):
    def get(self, response):
        collaborators = collaborators_list()
        return Response(
            data=CollaboratorSerializer(collaborators, many=True).data,
            status=200 if collaborators else 204,
        )


class CollaboratorsReadApi(APIView):
    class OutputSerializer(serializers.ModelSerializer):
        accounts = SimpleAccountSerializer(many=True)

        class Meta:
            model = Collaborator
            fields = "__all__"

    def get(self, request, collaborator_id):
        collaborator = collaborator_get(collaborator_id=collaborator_id)
        return (
            Response(self.OutputSerializer(collaborator).data, status=200)
            if collaborator
            else Response({"error": "id does not exist"}, status=400)
        )


class CollaboratorCreateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=128)
        salary = serializers.CharField(max_length=128, allow_blank=True)
        email = serializers.EmailField(max_length=128, allow_blank=True)
        contact_phone = serializers.CharField(max_length=128, allow_blank=True)
        emergency_contact = serializers.CharField(max_length=128, allow_blank=True)
        birthday = serializers.DateTimeField(allow_null=True)
        gender = serializers.CharField(max_length=128, allow_blank=True)

    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        collaborator = create_collaborator(**serializer.validated_data)
        return Response(data=CollaboratorSerializer(collaborator).data, status=201)


class CollaboratorUpdateApi(APIView):
    class InputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=128)
        salary = serializers.CharField(max_length=128, allow_blank=True)
        email = serializers.EmailField(max_length=128, allow_blank=True)
        contact_phone = serializers.CharField(max_length=128, allow_blank=True)
        emergency_contact = serializers.CharField(max_length=128, allow_blank=True)
        birthday = serializers.DateTimeField(allow_null=True)
        gender = serializers.CharField(max_length=128, allow_blank=True)

    def put(self, request, collaborator_id):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        collaborator = update_collaborator(
            **serializer.validated_data, collaborator_id=collaborator_id
        )

        return (
            Response(CollaboratorSerializer(data=collaborator).data, status=201)
            if collaborator
            else Response({"error": "id does not exist"}, status=400)
        )
