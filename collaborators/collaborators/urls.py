from django.urls import path

from .views import CollaboratorCreateApi
from .views import CollaboratorsListApi
from .views import CollaboratorsReadApi
from .views import CollaboratorUpdateApi

urlpatterns = [
    path("list/", CollaboratorsListApi.as_view()),
    path("read/<collaborator_id>/", CollaboratorsReadApi.as_view()),
    path("update/<collaborator_id>/", CollaboratorUpdateApi.as_view()),
    path("create/", CollaboratorCreateApi.as_view()),
]
