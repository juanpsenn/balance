from collaborators.models import Collaborator


def create_collaborator(
    *,
    name: str,
    salary: str,
    email: str,
    contact_phone: str,
    emergency_contact: str,
    birthday: str,
    gender: str
) -> Collaborator:

    collaborator = Collaborator.objects.create(
        name=name,
        salary=salary,
        email=email,
        contact_phone=contact_phone,
        emergency_contact=emergency_contact,
        birthday=birthday,
        gender=gender,
    )
    return collaborator


def update_collaborator(
    *,
    collaborator_id: int,
    name: str,
    salary: str,
    email: str,
    contact_phone: str,
    emergency_contact: str,
    birthday: str,
    gender: str
):
    try:
        collaborator = Collaborator.objects.get(pk=collaborator_id)
        collaborator.name = name
        collaborator.salary = salary
        collaborator.email = email
        collaborator.contact_phone = contact_phone
        collaborator.emergency_contact = emergency_contact
        collaborator.birthday = birthday
        collaborator.gender = gender
        collaborator.save()
        return collaborator
    except Collaborator.DoesNotExist:
        return None
