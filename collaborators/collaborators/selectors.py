from collaborators.models import Collaborator


def collaborators_list():
    return Collaborator.objects.filter(is_active=True)


def collaborator_get(*, collaborator_id) -> Collaborator:
    try:
        collaborator = Collaborator.objects.get(pk=collaborator_id)
        return collaborator
    except Collaborator.DoesNotExist:
        return None
